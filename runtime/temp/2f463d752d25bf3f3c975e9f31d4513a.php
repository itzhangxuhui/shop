<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:72:"D:\phpstudy_pro\WWW\web\public/../application/admin\view\role\index.html";i:1617875830;s:58:"D:\phpstudy_pro\WWW\web\application\admin\view\layout.html";i:1619683753;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="/public/static/admin/css/main.css" rel="stylesheet" type="text/css"/>
    <link href="/public/static/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/public/static/admin/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <script src="/public/static/admin/js/jquery-1.8.1.min.js"></script>
<!--    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.js"></script>-->
    <script src="/public/static/admin/js/bootstrap.min.js"></script>
<!--    <script src="/public/static/admin/js/highcharts.js"></script>-->
<!--    <script src="/public/static/admin/js/exporting.js"></script>-->
<!--    <script src="/public/static/admin/js/draw.js"></script>-->
<!--    <script src="/public/static/plugins/layer/layer.js"></script>-->

</head>
<body>
<!-- 上 -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <ul class="nav pull-right">
                <li id="fat-menu" class="dropdown">
                    <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-user icon-white"></i> admin
                        <i class="icon-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="javascript:void(0);">修改密码</a></li>
                        <li class="divider"></li>
                        <li><a tabindex="-1" href="javascript:void(0);">安全退出</a></li>
                    </ul>
                </li>
            </ul>
            <a class="brand" href="index.html"><span class="first">后台管理系统</span></a>
            <ul class="nav">
                <li class="active"><a href="javascript:void(0);">首页</a></li>
                <li><a href="javascript:void(0);">系统管理</a></li>
                <li><a href="javascript:void(0);">权限管理</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- 左 -->
<div class="sidebar-nav">
    <?php if(is_array($authInfoList) || $authInfoList instanceof \think\Collection || $authInfoList instanceof \think\Paginator): $i = 0; $__LIST__ = $authInfoList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$auil): $mod = ($i % 2 );++$i;?>
        <a href="#accounts-menu<?php echo $auil['id']; ?>" class="nav-header" data-toggle="collapse"><i class="icon-exclamation-sign"></i>
            <?php echo $auil['auth_name']; ?></a>
        <ul id="accounts-menu<?php echo $auil['id']; ?>" class="nav nav-list collapse in">

            <?php if(isset($auil['son'])): if(is_array($auil['son']) || $auil['son'] instanceof \think\Collection || $auil['son'] instanceof \think\Paginator): $i = 0; $__LIST__ = $auil['son'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$auilson): $mod = ($i % 2 );++$i;if($auilson['is_nav'] == '1'): ?>
                <li><a href="<?php echo url("$auilson[auth_c]/$auilson[auth_a]"); ?>"><?php echo $auilson['auth_name']; ?></a></li>
            <?php endif; endforeach; endif; else: echo "" ;endif; endif; ?>
        </ul>
    <?php endforeach; endif; else: echo "" ;endif; ?>
</div>
<!-- 右 -->
<div class="content">
    <div class="header">
        <h1 class="page-title">角色列表</h1>
    </div>

    <div class="well">
        <a class="btn btn-primary" href="<?php echo url('add'); ?>">新增</a>
        <!-- table -->
        <table class="table table-bordered table-hover table-condensed">
            <thead>
            <tr>
                <th>编号</th>
                <th>用户组</th>
                <th>权限</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php if(is_array($roleList) || $roleList instanceof \think\Collection || $roleList instanceof \think\Paginator): $i = 0; $__LIST__ = $roleList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ro): $mod = ($i % 2 );++$i;$color = $i%2==1?'success':'error'; ?>
            <tr class="<?php echo $color; ?>">
                <td><?php echo $i; ?></td>
                <td><?php echo $ro['role_name']; ?></td>
                <td><?php echo $ro['auth_name']; ?></td>
                <td>
                    <a href="<?php echo url('setAuth',['id'=>$ro['id'],'role_name'=>$ro['role_name']]); ?>"> 分派权限 </a>
                    <a href="javascript:void(0);" onclick="if(confirm('确认删除？')) location.href='#'"> 删除 </a>
                </td>
            </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
        <!-- pagination -->
        <div class="pagination">
            <ul>
                <li><a href="#">Prev</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">Next</a></li>
            </ul>
        </div>
    </div>
    <!-- footer -->
    <footer>
        <hr>
        <p>© 2017 <a href="javascript:void(0);" target="_blank">ADMIN</a></p>
    </footer>
</div>
</body>
</html>