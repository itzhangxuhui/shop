<?php

namespace app\Home\controller;

use think\Controller;
use think\Cookie;
use think\Db;

class Product extends Controller
{
    // 商品详情页 需要传递一个商品id
    public function index($id){
        $goods = Db::name("goods")->find($id);
        // 1.查询cookie是否已经被存过，如果存过了，则不存
        /**
         * [
         *  ['id'=>1,"goods_name"=>"AAA","goods_price"=>88.88],
         *  []
         * ]
         */
        Cookie::init(['prefix'=>'myshop_','expire'=>6000,'path'=>'/']);
        // 1.从cookie中获取数据
        $data = Cookie::get("goods");
        $goods['lasttime'] = time();
        $data["id_".$goods['id']] = $goods;

        Cookie::set("goods",$data);
        $history = Cookie::get("goods");
        dump($history);
        dump("-----------");
        $temp_arr = array_column($history, 'lasttime');
        array_multisort($temp_arr, SORT_DESC, $history);

           if(count($history)>2){
               array_pop($history);
           }
           // cookie全删
        Cookie::set("goods",$history);
       dump($history);
//        dump(Cookie::get("hello"));

        $data = compact("goods");
//        return view("",$data);
    }


    public function list(){
        // ajax 无刷新分页
        $goodsList = Db::name("goods")->limit(3)->select();
        $totalPage = ceil(Db::name("goods")->count()/3);
        return view("",['goodsList'=>$goodsList,"totalPage"=>$totalPage]);
    }

    // ajax 分页 处理
    public function ajaxReturnPage(){
        $page = input("get.page");
        $pageSize = 3;
        $start = ($page-1)*$pageSize;
        $goodsList = Db::name("goods")->limit($start,$pageSize)->select();
        return json($goodsList);
    }
}
