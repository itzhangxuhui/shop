<?php

namespace app\home\controller;

use app\Common\model\Article;
use think\Collection;
use think\Controller;
use think\Db;
use app\home\model\Member as ME;
use app\home\model\MemberProfile as MP;

class Member extends Controller
{
    //
    public function index(){
       /* $memberinfo = Db::name("member m")->join("member_profile mp","m.id=mp.mid")
        ->field("m.*,mp.hobby,mp.height")->where("m.id=110")->find();
        dump($memberinfo);
        echo "<hr>";
      $memberinfo = Db::name("member m")->join("member_profile mp","m.id=mp.mid")
            ->field("m.*,mp.hobby,mp.height")->select();
        dump($memberinfo);*/
        // toArray 将对象--》数组
//        dump(ME::find(110)->toArray());
      /*  $me = new ME();
        $row = $me->name("member m")->join("member_profile mp","m.id=mp.mid")
            ->field("m.*,mp.hobby,mp.height")->where("m.id=110")->find();
        dump($row);*/
        //1.一对一 正向 主表-》副表
//        $me = new ME();
        //2.获取记录
        // 第一张表记录
//        $info1 = ME::find(110);
        // 条件是主表  主键
        // 查询一条记录
//        $info3 = ME::with("memberProfile")->find(110)->toArray();
//        dump($info3);
        // 查询多条记录
        $info3 = ME::with("memberProfile")->select();

//        return json($info3);
//        dump($info3);
        $result = new Collection($info3);
//        dump($result);
        dump($result->toArray());
//        return json($info3);
//        dump($info1->toArray());
        // 第二张表记录
//        $info2 = $info1->profile->toArray();
//        dump($info2);
//        return json($info1->profile);
    }

    // 一对一 反向使用 副表-》主表
    public function index1(){  // 条件 是副表的 主键
        $info3 = MP::with("member")->find(3);
        return json($info3);
    }
    // 正向 一对一 新增
    public function add(){
        $user = ME::find(118);
        // 如果还没有关联数据 则进行新增
        // memberProfile() 新增
//        $user->memberProfile()->save(['hobby' => 'xuexi123','height'=>188]);
        // memberProfile 更新
        $user->memberProfile->save(['hobby' => '学习','height'=>188]);

    }
    // 使用公共模型
    public function show(){
        $a = new Article();
        $a->show();
    }
}
