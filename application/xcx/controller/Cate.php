<?php

namespace app\xcx\controller;
use think\Db;
use think\Controller;
use think\Request;

class Cate extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        // 获取所有的一级分类
        $rows = Db::name("category")->where("pid",0)->select();
        return json($rows);
    }
    // 根据一级分类id 获取二级分类的名称和商品
    /**
     * [
     *  ["二级分类名称"]=>[[商品1],[商品2],[商品3]]
     * ["二级分类名称"]=>[[商品1],[商品2],[商品3]]
     * ]
     *
     *
     */
    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
