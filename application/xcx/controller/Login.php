<?php

namespace app\xcx\controller;

use app\admin\model\Fans;
use think\Controller;
use think\Request;
use think\Db;
class login extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $code = input("get.code");
//        $code = 123;
        $appid = "wx4d813f1fef508ddf";
        $secret = "c9c9e287819559eecf0fff650b74bb7a";
        //1. 根据code来换取openid
        $url  = "https://api.weixin.qq.com/sns/jscode2session?appid=$appid&secret=$secret&js_code=$code&grant_type=authorization_code";


        // string(83) "{"session_key":"XERQtY75AlLq1+R\/3y2lKg==","openid":"oxzAm0Tb1QXOehYnWo26MUwmoFq4"}"
        // string(78) "{"errcode":40029,"errmsg":"invalid code, hints: [ req_id: NGfcc6qNe-RIrUDA ]"}"
        $res = json_decode(curl($url),true);
        $openid = $res['openid'];
        if($openid){
            // openid 存在
            // 2. 根据openid 来查询 ，如果查到了，说明此用户曾经登录过，如果查不到，说明从未登录过
            if($row = Db::name("fans")->where("xcx_openid",$openid)->find()){
                // 如果查到了，就说明此人登陆过，我们已经获取了他的所有信息了
                // 返回用户的id ，返回 openid，返回头像，昵称
//            return ?
                $data['status'] = 0;
                $data['info'] = $row;

            }else{
                $data['status'] = 1; // 數據表不存在
                $data['openid'] = $openid;
            }
        }else{
            $data['status'] = 2;
        }

       return json($data);



    }

    // 用户同意授权后，点击了允许，信息写入数据库
    public function addFans(){

        $avatarUrl = input("get.avatarUrl");
        $city = input("get.city");
        $country = input("get.country");
        $gender = input("get.gender");
        $nickName = input("get.nickName");
        $openid = input("get.openid");
        // 查询是否已经存在
        if($row = Db::name("fans")->where("xcx_openid",$openid)->find()){
            $data['status'] = 0;
            $data['fid'] = $row['id'];
        }else{
            // 写数据库
            $fans = Fans::create([
                'headimgurl'  =>  $avatarUrl,
                'city' =>  $city,
                'country' =>  $country,
                'sex' =>  $gender,
                'nickname' =>  $nickName,
                'xcx_openid' =>  $openid,
            ]);
            if($fans->id){ // 获取自增ID
                // 數據寫入成功
                $data['status'] = 0;
                $data['fid'] = $fans->id;
            } else{
                // 數據寫入失敗
                $data['status'] = 1;

            }
        }

        return json($data);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
