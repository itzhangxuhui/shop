<?php

namespace app\admin\model;

use think\Model;

class Category extends Model
{
     // 开始自动维护时间戳
        protected $autoWriteTimestamp = true;


        // 一对多 正向操作
//hasMany('关联模型名','外键名','主键名',['模型别名定义']);
// bind　不能用
    public function goods(){
        return $this->hasMany('Goods',"cate_id");
    }
}
