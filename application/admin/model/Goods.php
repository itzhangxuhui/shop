<?php

namespace app\Admin\model;

use think\Model;

class Goods extends Model
{
    // 反向一对多
    public function category(){
        //belongsTo('关联模型名','外键名','关联表主键名',['模型别名定义'],'join类型');
        return $this->belongsTo("Category","cate_id","id")->bind("cate_name");
    }
}
