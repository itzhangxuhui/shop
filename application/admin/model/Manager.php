<?php

namespace app\Admin\model;

use think\Config;
use think\Db;
use think\Model;

class Manager extends Model
{
    //    // 开始自动维护时间戳
   protected $autoWriteTimestamp = true;

   // 搜索 展示 管理员列表

    public function searchOrShowList($mid,$map,$query=[],$orderby="create_time"){
        $list_rows =  Config::get("paginate.list_rows");
        return Db::name("manager m")->join("manager_role mr","m.id=mr.mid","LEFT")->join("role r","mr.rid=r.id","LEFT")->field("m.*,r.role_name,r.id rid")->where("m.id",">",1)->where("m.id","<>",$mid)->where($map)->order(["$orderby"=>"desc"])->paginate($list_rows,false,['query' => $query]);
    }
}
