<?php

namespace app\admin\controller;

use think\Controller;

// 素材管理
class Media extends Common
{
    // 素材列表
    public function index(){

    }
    // 1.新增素材的页面
    public function add(){
       /* */
//        $data = compact("access_token");
        return view();
    }
    // 上传素材
    public function addAction(){
        // 获取下 用户选择的类型
        $type = input("post.type");
        // 1.tp的文件上传 上传到公众号的服务器
        $file = request()->file('pic');
           // 移动到框架应用根目录/public/uploads/ 目录下
     $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
    if($info){
        // 成功上传后 获取上传信息
        // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
//        echo $info->getSaveName();
        $filename = "./uploads/".$info->getSaveName();
        // 新增临时素材
//        $this->addTempMedia($filename,$type);
        // 新增永久素材
        $this->addForverMedia($filename,$type);
   }else{
                    // 上传失败获取错误信息
       echo $file->getError();
   }


    }
    // 新增临时素材
    public function addTempMedia($filename,$type){
        // 2.上传到微信的服务器，新增永久素材 curl 请求  (临时素材)
        $weichat = new Wechat();
        $access_token = $weichat->createAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=$access_token&type=$type";
        $data = ['media'=>new \CURLFile($filename)];
        dump(curl($url,$data,1));
        // 入库
    }
    // 获取临时素材
    public function getTempMedia(){
        $weichat = new Wechat();
        $access_token = $weichat->createAccessToken();
        $mediaid = "DFuW--DirvW6KCNDXcKuHbVorPOpsaOBWBYzShvt4x4Ye5bPKfLjE6LFO7-YKd0j";
        $url = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=$access_token&media_id=$mediaid";
        file_put_contents("aaa.jpg",curl($url));
    }

    // 新增永久素材
    public function addForverMedia($filename,$type){
        $weichat = new Wechat();
        $access_token = $weichat->createAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=$access_token&type=$type";
        $data = ['media'=>new \CURLFile($filename)];
        dump(curl($url,$data,1));
        // 删除我的服务器上的文件 阅后即焚
        unlink($filename);
        // 入库

    }

    // 获取永久素材
    public function getForverMedia(){
        $weichat = new Wechat();
        $access_token = $weichat->createAccessToken();
        $mediaid = "DFuW--DirvW6KCNDXcKuHbVorPOpsaOBWBYzShvt4x4Ye5bPKfLjE6LFO7-YKd0j";
        $url = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=$access_token&media_id=$mediaid";
        file_put_contents("aaa.jpg",curl($url));
    }

}
