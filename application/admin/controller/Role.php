<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;
use app\admin\model\Role as RO;
class Role extends Common
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //获取所有的角色
        $role = Db::name("role r")->join("role_auth ra","ra.rid=r.id","LEFT")->join("auth a","a.id = ra.aid","LEFT")->field("r.role_name,r.id,a.auth_name")->select();
        $roleList = [];
        foreach ($role as $v){
           if(!array_key_exists($v['id'],$roleList)){
               $roleList[$v['id']] = $v;
           }else{
               $roleList[$v['id']]['auth_name'] .= ",".$v['auth_name'] ;
           }
        }
        return view("",['roleList'=>$roleList]);
    }

    // 添加角色
    public function add(){
        return view();
    }
    // 添加角色处理
    public function addAction(){
        $role_name = input("post.role_name");
        // 查
        // 根据字段查询用户
        if(RO::getByRoleName($role_name)){
            $this->error("添加失败");
        }
        // 添加
        if(RO::create(['role_name'=>$role_name])){
            $this->success("角色添加成功");
        }


    }

    // 分配权限
    public function setAuth($id,$role_name){
        // 获取所有的权限
        $authList = generateTree(getArr());

        // 查询当前角色拥有的所有权限
        $aids = Db::name("role_auth")->where("rid",$id)->column("aid");
        // in_array 判断数组中是否有某个值
//        dump(in_array(23,$aids));
//        dump(in_array(24,$aids));
        $data = compact("id","role_name","authList","aids");
        return view("",$data);
    }
    public function addAjax(){
        $role_name = input("post.role_name");
        if(RO::getByRoleName($role_name)){
            return false;
        }else{
            return true;
        }
    }

    // 分配权限处理
    public function setAuthAction(){
        $data = input();
        $rid = $data['rid'];
        Db::name("role_auth")->where("rid",$rid)->delete();
        if(!array_key_exists("id",$data)){
            return $this->success("权限编辑成功","index");
        }
        $aids = $data['id'];
        foreach ($aids as $aid){
            Db::name("role_auth")->insert(['rid'=>$rid,'aid'=>$aid]);
        }
        $this->success("权限编辑成功","index");

    }
}
